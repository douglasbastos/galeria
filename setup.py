#coding: utf-8
from setuptools import setup, find_packages
setup(
    name ='galeria',
    version='0.3',
    description='minha galeria de imagens.',
    author='Douglas Bastos',
    author_email='douglas.bastos@corp.globo.com',
    url='http://localhost:8000',
    packages=find_packages(),
    long_description="minha galeria.",
    classifiers=[
        "Development Status :: 1 - Planning",
        "Programming Language :: Python :: 2.7",
    ],
    keywords='palavras chave separadas por espaco',
    license='glp',
    include_package_data=True,
)
