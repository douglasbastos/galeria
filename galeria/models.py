from django.db import models
import datetime
from django.utils import timezone

class Galeria(models.Model):
    title = models.CharField(max_length=50)
    tag = models.CharField(max_length=20)

    def __unicode__(self):
        return self.title

class Foto(models.Model):
    galeria = models.ForeignKey(Galeria)
    image_url = models.URLField()
    descricao = models.CharField(max_length=50)

    def __unicode__(self):
        return self.descricao

