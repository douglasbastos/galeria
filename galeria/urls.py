from django.conf.urls import patterns, url

from galeria import views

urlpatterns = patterns('',
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^(?P<pk>\d+)/fotos$', views.FotosView.as_view(), name='fotos'),
)