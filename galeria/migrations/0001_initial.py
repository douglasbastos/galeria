# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Galeria'
        db.create_table(u'galeria_galeria', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('tag', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal(u'galeria', ['Galeria'])

        # Adding model 'Foto'
        db.create_table(u'galeria_foto', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('galeria', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['galeria.Galeria'])),
            ('image_url', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('descricao', self.gf('django.db.models.fields.CharField')(max_length=55)),
        ))
        db.send_create_signal(u'galeria', ['Foto'])


    def backwards(self, orm):
        # Deleting model 'Galeria'
        db.delete_table(u'galeria_galeria')

        # Deleting model 'Foto'
        db.delete_table(u'galeria_foto')


    models = {
        u'galeria.foto': {
            'Meta': {'object_name': 'Foto'},
            'descricao': ('django.db.models.fields.CharField', [], {'max_length': '55'}),
            'galeria': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['galeria.Galeria']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        u'galeria.galeria': {
            'Meta': {'object_name': 'Galeria'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tag': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['galeria']