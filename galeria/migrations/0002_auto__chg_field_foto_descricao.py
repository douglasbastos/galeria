# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Foto.descricao'
        db.alter_column(u'galeria_foto', 'descricao', self.gf('django.db.models.fields.CharField')(max_length=50))

    def backwards(self, orm):

        # Changing field 'Foto.descricao'
        db.alter_column(u'galeria_foto', 'descricao', self.gf('django.db.models.fields.CharField')(max_length=55))

    models = {
        u'galeria.foto': {
            'Meta': {'object_name': 'Foto'},
            'descricao': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'galeria': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['galeria.Galeria']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image_url': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        },
        u'galeria.galeria': {
            'Meta': {'object_name': 'Galeria'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'tag': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        }
    }

    complete_apps = ['galeria']