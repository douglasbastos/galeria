from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic

from galeria.models import Foto, Galeria


class FotosView(generic.ListView):
    model = Foto
    template_name = 'galeria/fotos.html'

    def get_queryset(self):
        queryset = super(generic.ListView, self).get_queryset()
        return queryset.filter(galeria=self.kwargs['pk'])


class IndexView(generic.ListView):
    model = Galeria
    template_name = 'galeria/index.html'

    def get_context_data(self, **kwargs):
        dados = super(generic.ListView, self).get_context_data(**kwargs)
        dados['titulo'] = 'Galeria'
        return dados