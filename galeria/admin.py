from django.contrib import admin
from galeria.models import Foto, Galeria

class FotoInline(admin.TabularInline):
    model = Foto
    extra = 1

class GaleriaAdmin(admin.ModelAdmin):
    model = Galeria
    inlines = [FotoInline]

admin.site.register(Galeria, GaleriaAdmin)


